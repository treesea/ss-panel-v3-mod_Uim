<?php
/**
 * Created by Vscode.
 * User: maooyer
 * Date: 2019/01/01
 * Time: 下午10:24
 */

namespace App\Services\Gateway;

use App\Services\Auth;
use App\Services\Config;
use App\Models\Code;
use App\Models\Shop;
use App\Models\Paylist;
use App\Services\View;


use ReceiptValidator\iTunes\Validator as iTunesValidator;
use ReceiptValidator\iTunes\Response as ValidatorResponse;

class ApplePay extends AbstractPayment
{
    private function createGateway(){
        $gateway = Omnipay::create('Alipay_AopF2F');
        $gateway->setSignType('RSA2'); //RSA/RSA2
        $gateway->setAppId(Config::get("f2fpay_app_id"));
        $gateway->setPrivateKey(Config::get("merchant_private_key")); // 可以是路径，也可以是密钥内容
        $gateway->setAlipayPublicKey(Config::get("alipay_public_key")); // 可以是路径，也可以是密钥内容
        $gateway->setNotifyUrl(Config::get("baseUrl")."/payment/notify");

        return $gateway;
    }


    function purchase($request, $response, $args)
    {
        $product_id = $request->getParam('product_id');
        //$pay_method = $pay_method->getParam('pay_method');
        
        // $user = Auth::getUser();
        // // 验证产品号正常
        // $shop = Shop::where("id", $product_id)->first();
        // if($shop){
        //     $pl = new Paylist();
        //     $pl->userid = $user->id;
        //     $pl->tradeno = self::generateGuid();
        //     $pl->total = $shop->price;
        //     $pl->save();
        // }
        

        $return['ret'] = 0;
        $return['msg'] = "ok"; 
        $return['data']['product_id'] = $product_id;
        // $return['tradeno'] = $pl->tradeno;
        // $return['price'] = $shop->price;
        

        return json_encode($return);
    }

    function notify($request, $response, $args)
    {
        $validator = new iTunesValidator(iTunesValidator::ENDPOINT_SANDBOX);
        $receiptData = $request->getParam("receipt-data");

        try {
            $appleResponse = $validator->setReceiptData($receiptData)->validate();

        } catch (\Exception $e) {
            die('fail');
        }
        if ($response instanceof ValidatorResponse && $response->isValid()) {
            $return['ret'] = 0;
            $return['msg'] = "ok";
        }
        else {
            $return['ret'] = 1001;
            $return['msg'] = "Receipt Invaild";
        }
    }


    function getPurchaseHTML()
    {
        return View::getSmarty()->fetch("user/aopf2f.tpl");
    }

    function getReturnHTML($request, $response, $args)
    {
        return 0;
    }

    function getStatus($request, $response, $args)
    {
        $p = Paylist::where("tradeno", $_POST['pid'])->first();
        $return['ret'] = 1;
        $return['result'] = $p->status;
        return json_encode($return);
    }
}