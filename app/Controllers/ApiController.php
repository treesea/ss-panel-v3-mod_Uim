<?php

namespace App\Controllers;

use App\Models\InviteCode;
use App\Services\Config;
use App\Utils\Check;
use App\Utils\Tools;
use App\Utils\Radius;
use voku\helper\AntiXSS;
use App\Utils\Hash;
use App\Utils\Helper;

use App\Models\Node;
use App\Models\User;
use App\Models\Shop;
use App\Models\Paylist;
use App\Services\Factory;
use App\Services\Mail;
use App\Models\LoginIp;
use App\Models\EmailVerify;

use ReceiptValidator\iTunes\Validator as iTunesValidator;
use ReceiptValidator\iTunes\Response as ValidatorResponse;

/**
 *  ApiController
 */

class ApiController extends BaseController
{
    public function index()
    { }

    public function token($request, $response, $args)
    {
        $accessToken = $id = $args['token'];
        $storage = Factory::createTokenStorage();
        $token = $storage->get($accessToken);

        if ($token == null) {
            $res['ret'] = 2001;
            $res['msg'] = "token is null";
            return $this->echoJson($response, $res);
        }
        $res['ret'] = 0;
        $res['msg'] = "ok";
        $res['data'] = $token;
        return $this->echoJson($response, $res);
    }

    public function newToken($request, $response, $args)
    {
        // $data = $request->post('sdf');
        $email =  $request->getParam('email');

        $email = strtolower($email);
        $passwd = $request->getParam('passwd');

        // Handle Login
        $user = User::where('email', '=', $email)->first();

        if ($user == null) {
            $res['ret'] = 0;
            $res['msg'] = "401 邮箱或者密码错误";
            return $this->echoJson($response, $res);
        }

        if (!Hash::checkPassword($user->pass, $passwd)) {
            $res['ret'] = 0;
            $res['msg'] = "402 邮箱或者密码错误";
            return $this->echoJson($response, $res);
        }
        $tokenStr = Tools::genToken();
        $storage = Factory::createTokenStorage();
        $expireTime = time() + 3600 * 24 * 7;
        if ($storage->store($tokenStr, $user, $expireTime)) {
            $res['ret'] = 1;
            $res['msg'] = "ok";
            $res['data']['token'] = $tokenStr;
            $res['data']['user_id'] = $user->id;
            return $this->echoJson($response, $res);
        }
        $res['ret'] = 0;
        $res['msg'] = "system error";
        return $this->echoJson($response, $res);
    }

    public function register($request, $response, $args)
    {
        if(Config::get('register_mode')=='close') {
            $res['ret'] = 1001;
            $res['msg'] =  'not open';
        }
        // 取得参数
        $name = $request->getParam('username');
        $passwd = $request->getParam('password');
        $email = $request->getparam('email');
        $email = trim($email);
        $email = strtolower($email);
        
        // 验证码
        if (Config::get('enable_reg_captcha') == 'true') {
            switch(Config::get('captcha_provider'))
            {
                case 'recaptcha':
                    $recaptcha = $request->getParam('recaptcha');
                    if ($recaptcha == ''){
                        $ret = false;
                    }else{
                        $json = file_get_contents("https://recaptcha.net/recaptcha/api/siteverify?secret=".Config::get('recaptcha_secret')."&response=".$recaptcha);
                        $ret = json_decode($json)->success;
                    }
                    break;
                case 'geetest':
                    $ret = Geetest::verify($request->getParam('geetest_challenge'), $request->getParam('geetest_validate'), $request->getParam('geetest_seccode'));
                    break;
            }
            if (!$ret) {
                $res['ret'] = 0;
                $res['msg'] = "系统无法接受您的验证结果，请刷新页面后重试。";
                return $response->getBody()->write(json_encode($res));
            }
        }

        // check email format
        if (!Check::isEmailLegal($email)) {
            $res['ret'] = 0;
            $res['msg'] = "邮箱无效";
            return $response->getBody()->write(json_encode($res));
        }

        // check email
        $user = User::where('email', $email)->first();
        if ($user != null) {
            $res['ret'] = 0;
            $res['msg'] = "邮箱已经被注册了";
            return $response->getBody()->write(json_encode($res));
        }

        if (Config::get('enable_email_verify') == 'true') {
            $mailcount = EmailVerify::where('email', '=', $email)->where('code', '=', $emailcode)->where('expire_in', '>', time())->first();
            if ($mailcount == null) {
                $res['ret'] = 0;
                $res['msg'] = "您的邮箱验证码不正确";
                return $response->getBody()->write(json_encode($res));
            }
        }

        // check pwd length
        if (strlen($passwd) < 8) {
            $res['ret'] = 0;
            $res['msg'] = "密码请大于8位";
            return $response->getBody()->write(json_encode($res));
        }

        // 创建用户
        $user = new User();

        $antiXss = new AntiXSS();

        $user->user_name = $antiXss->xss_clean($name);
        $user->email = $email;
        $user->pass = Hash::passwordHash($passwd);
        $user->passwd = Tools::genRandomChar(6);
        $user->port = Tools::getAvPort();
        $user->t = 0;
        $user->u = 0;
        $user->d = 0;
        $user->method = Config::get('reg_method');
        $user->protocol = Config::get('reg_protocol');
        $user->protocol_param = Config::get('reg_protocol_param');
        $user->obfs = Config::get('reg_obfs');
        $user->obfs_param = Config::get('reg_obfs_param');
        $user->forbidden_ip = Config::get('reg_forbidden_ip');
        $user->forbidden_port = Config::get('reg_forbidden_port');
        #$user->im_type = $imtype;
        #$user->im_value = $antiXss->xss_clean($wechat);
        $user->transfer_enable = Tools::toGB(Config::get('defaultTraffic'));
        $user->invite_num = Config::get('inviteNum');
        $user->auto_reset_day = Config::get('reg_auto_reset_day');
        $user->auto_reset_bandwidth = Config::get('reg_auto_reset_bandwidth');
        $user->money = 0;

        $user->class_expire = date("Y-m-d H:i:s", time() + Config::get('user_class_expire_default') * 3600);
        $user->class = Config::get('user_class_default');
        $user->node_connector = Config::get('user_conn');
        $user->node_speedlimit = Config::get('user_speedlimit');
        $user->expire_in = date("Y-m-d H:i:s", time() + Config::get('user_expire_in_default') * 86400);
        $user->reg_date = date("Y-m-d H:i:s");
        $user->reg_ip = $_SERVER["REMOTE_ADDR"];
        $user->plan = 'A';
        $user->theme = Config::get('theme');

        $groups=explode(",", Config::get('ramdom_group'));

        $user->node_group=$groups[array_rand($groups)];

        $ga = new GA();
        $secret = $ga->createSecret();

        $user->ga_token = $secret;
        $user->ga_enable = 0;

        if ($user->save()) {
            $res['ret'] = 0;
            $res['msg'] = "注册成功";
            Radius::Add($user, $user->passwd);
            return $response->getBody()->write(json_encode($res));
        }

        $res['ret'] = 4000;
        $res['msg'] = "未知错误";
        return $this->echoJson($response, $res);
    }

    public function requestEmailVerify($request, $response, $args)
    {
        $email = $request->getParam('email');
		$email = trim($email);

        if ($email == "") {
            $res['ret'] = 1;
            $res['msg'] = "未填写邮箱";
            return $this->echoJson($response, $res);
        }

        // check email format
        if (!Check::isEmailLegal($email)) {
            $res['ret'] = 1;
            $res['msg'] = "邮箱无效";
            return $this->echoJson($response, $res);
        }

        $user = User::where('email', '=', $email)->first();
        if ($user != null) {
            $res['ret'] = 1;
            $res['msg'] = "此邮箱已经注册";
            return $this->echoJson($response, $res);
        }

        $ipcount = EmailVerify::where('ip', '=', $_SERVER["REMOTE_ADDR"])->where('expire_in', '>', time())->count();
        if ($ipcount >= (int)Config::get('email_verify_iplimit')) {
            $res['ret'] = 1;
            $res['msg'] = "此IP请求次数过多";
            return $this->echoJson($response, $res);
        }


        $mailcount = EmailVerify::where('email', '=', $email)->where('expire_in', '>', time())->count();
        if ($mailcount >= 3) {
            $res['ret'] = 1;
            $res['msg'] = "此邮箱请求次数过多";
            return $this->echoJson($response, $res);
        }

        $code = Tools::genRandomNum(6);

        $ev = new EmailVerify();
        $ev->expire_in = time() + Config::get('email_verify_ttl');
        $ev->ip = $_SERVER["REMOTE_ADDR"];
        $ev->email = $email;
        $ev->code = $code;
        $ev->save();

        $subject = Config::get('appName') . "- 验证邮件";

        try {
            Mail::send($email, $subject, 'auth/verify.tpl', [
                "code" => $code, "expire" => date("Y-m-d H:i:s", time() + Config::get('email_verify_ttl'))
            ], [
                //BASE_PATH.'/public/assets/email/styles.css'
            ]);
        } catch (\Exception $e) {
            $res['ret'] = 1;
            $res['msg'] = "邮件发送失败，请联系网站管理员。";
            return $this->echoJson($response, $res);
        }

        $res['ret'] = 0;
        $res['msg'] = "ok";

        return $this->echoJson($response, $res);
    }

    public function forgetPassword($request, $response, $args)
    {
        // 待实现
        $email = $request->getParam('email');
        $res['ret'] = $email;
        return $this->echoJson($response, $res);
    }

    public function resetPassword($request, $response, $args)
    {
        // 待实现
        $email = $request->getParam('email');
        $res['ret'] = $email;
        return $this->echoJson($response, $res);
    }


    public function node($request, $response, $args)
    {
        $accessToken = Helper::getTokenFromReq($request);
        $storage = Factory::createTokenStorage();
        $token = $storage->get($accessToken);
        $user = User::find($token->userId);
        $nodes = Node::where('sort', 0)->where("type", "1")->where(
            function ($query) use ($user) {
                $query->where("node_group", "=", $user->node_group)
                    ->orWhere("node_group", "=", 0);
            }
        )->get();

        $mu_nodes = Node::where('sort', 9)->where('node_class', '<=', $user->class)->where("type", "1")->where(
            function ($query) use ($user) {
                $query->where("node_group", "=", $user->node_group)
                    ->orWhere("node_group", "=", 0);
            }
        )->get();

        $temparray = array();
        foreach ($nodes as $node) {
            if ($node->mu_only == 0) {
                array_push($temparray, array(
                    "remarks" => $node->name,
                    "server" => $node->server,
                    "server_port" => $user->port,
                    "method" => ($node->custom_method == 1 ? $user->method : $node->method),
                    "obfs" => str_replace("_compatible", "", (($node->custom_rss == 1 && !($user->obfs == 'plain' && $user->protocol == 'origin')) ? $user->obfs : "plain")),
                    "obfsparam" => (($node->custom_rss == 1 && !($user->obfs == 'plain' && $user->protocol == 'origin')) ? $user->obfs_param : ""),
                    "remarks_base64" => base64_encode($node->name),
                    "password" => $user->passwd,
                    "tcp_over_udp" => false,
                    "udp_over_tcp" => false,
                    "group" => Config::get('appName'),
                    "protocol" => str_replace("_compatible", "", (($node->custom_rss == 1 && !($user->obfs == 'plain' && $user->protocol == 'origin')) ? $user->protocol : "origin")),
                    "obfs_udp" => false,
                    "enable" => true
                ));
            }

            if ($node->custom_rss == 1) {
                foreach ($mu_nodes as $mu_node) {
                    $mu_user = User::where('port', '=', $mu_node->server)->first();
                    $mu_user->obfs_param = $user->getMuMd5();

                    array_push($temparray, array(
                        "remarks" => $node->name . "- " . $mu_node->server . " 单端口",
                        "server" => $node->server,
                        "server_port" => $mu_user->port,
                        "method" => $mu_user->method,
                        "group" => Config::get('appName'),
                        "obfs" => str_replace("_compatible", "", (($node->custom_rss == 1 && !($mu_user->obfs == 'plain' && $mu_user->protocol == 'origin')) ? $mu_user->obfs : "plain")),
                        "obfsparam" => (($node->custom_rss == 1 && !($mu_user->obfs == 'plain' && $mu_user->protocol == 'origin')) ? $mu_user->obfs_param : ""),
                        "remarks_base64" => base64_encode($node->name . "- " . $mu_node->server . " 单端口"),
                        "password" => $mu_user->passwd,
                        "tcp_over_udp" => false,
                        "udp_over_tcp" => false,
                        "protocol" => str_replace("_compatible", "", (($node->custom_rss == 1 && !($mu_user->obfs == 'plain' && $mu_user->protocol == 'origin')) ? $mu_user->protocol : "origin")),
                        "obfs_udp" => false,
                        "enable" => true
                    ));
                }
            }
        }

        $res['ret'] = 1;
        $res['msg'] = "ok";
        $res['data'] = $temparray;
        return $this->echoJson($response, $res);
    }

    public function userInfo($request, $response, $args)
    {
        //$id = $args['id'];
        $accessToken = Helper::getTokenFromReq($request);
        $storage = Factory::createTokenStorage();
        $token = $storage->get($accessToken);

        // if ($id != $token->userId) {
        //     $res['ret'] = 0;
        //     $res['msg'] = "access denied";
        //     return $this->echoJson($response, $res);
        // }
        $user = User::find($token->userId);
        $user->pass = null;
        $data = $user;
        $res['ret'] = 1;
        $res['msg'] = "ok";
        $res['data'] = $data;
        return $this->echoJson($response, $res);
    }

    public function products($request, $response, $args)
    {
        $shops = Shop::where("status", 1)->orderBy("name")->get();
        $res['ret'] = 0;
        $res['msg'] = "ok";
        foreach ($shops as $item) {
            $item['content'] = json_decode($item['content'], true);
        }
        $res['data'] = $shops;
        return $this->echoJson($response, $res);
    }

    public function purchase($request, $response, $args)
    {
        // check parameters
        $pay_method = $request->getParam('pay_method');
        $product_id = $request->getParam('product_id');
        if ($pay_method == null || $product_id == null) {
            $res['ret'] = 2002;
            $res['msg'] = "missing parameters";
            return $this->echoJson($response, $res);
        }

        $accessToken = Helper::getTokenFromReq($request);
        $storage = Factory::createTokenStorage();
        $token = $storage->get($accessToken);

        $shop = Shop::where("id", $product_id)->first();
        if ($shop) {
            $pl = new Paylist();
            $pl->userid = $token->userId;
            $pl->tradeno = $pl->tradeno = self::generateGuid();
            $pl->total = $shop->price;
            $pl->save();

            $return['ret'] = 0;
            $return['msg'] = "ok";
            $return['data']['product_id'] = $product_id;
            $return['data']['tradeno'] = $pl->tradeno;
            $return['data']['price'] = $shop->price;
        } else {
            $return['ret'] = 2101;
            $return['msg'] = "product_id error";
        }
        return json_encode($return);
    }

    public function purchaseCallback($request, $response, $args)
    {
        $tradeno = $request->getParam('tradeno');
        $pay_method = $request->getParam('pay_method');
        $receiptData = $request->getParam("receipt-data");

        if ($tradeno == null || $pay_method == null || $receiptData == null) {
            $res['ret'] = 2002;
            $res['msg'] = "missing parameters";
            return $this->echoJson($response, $res);
        }

        $accessToken = Helper::getTokenFromReq($request);
        $storage = Factory::createTokenStorage();
        $token = $storage->get($accessToken);

        $pl = Paylist::where("tradeno", $tradeno)->first();
        if($pl) {

            if(($pl->userid  != $token->userId) || ($pl->status != 0)) {
                $return['ret'] = 2101;
                $return['msg'] = "tradeno error";
                $pl->save();
                return json_encode($return);
            }

            $validator = new iTunesValidator(iTunesValidator::ENDPOINT_SANDBOX);

            try {
                $appleResponse = $validator->setReceiptData($receiptData)->validate();
            } catch (\Exception $e) {
                echo 'got error = ' . $e->getMessage() . PHP_EOL;
            }
            //$appleResponse instanceof ValidatorResponse &&
            if ($appleResponse->isValid()) {
                $return['ret'] = 0;
                $return['msg'] = "ok";
                $pl->status = 1;
            } else {
                $return['ret'] = 2101;
                $return['msg'] = "receipt invaild";
            }

        } else {
            $return['ret'] = 2101;
            $return['msg'] = "tradeno error";
        }
        $pl->save();
        
        return json_encode($return);
    }

    public function orders($request, $response, $args)
    {
        $accessToken = Helper::getTokenFromReq($request);
        $storage = Factory::createTokenStorage();
        $token = $storage->get($accessToken);

        $pl = Paylist::where("userid", $token->userId);

        $return['ret'] = 0;
        $return['msg'] = "ok";
        $return['data'] = $pl;

        return json_encode($return);
    }

    public function test($request, $response, $args)
    {

        $return['ret'] = 0;
        $return['msg'] = "ok";
        //$user = Auth::getUser();
        //$return['data'] = $user;
        $accessToken = Helper::getTokenFromReq($request);
        $storage = Factory::createTokenStorage();
        $token = $storage->get($accessToken);
        $return['data'] = $token;
        return json_encode($return);
    }


    public static function generateGuid()
    {
        mt_srand((double)microtime() * 10000);
        $charid = strtoupper(md5(uniqid(rand() + time(), true)));
        $hyphen = chr(45);
        $uuid   = chr(123)
            . substr($charid, 0, 8) . $hyphen
            . substr($charid, 8, 4) . $hyphen
            . substr($charid, 12, 4) . $hyphen
            . substr($charid, 16, 4) . $hyphen
            . substr($charid, 20, 12)
            . chr(125);
        $uuid = str_replace(['}', '{', '-'], '', $uuid);
        $uuid = substr($uuid, 0, 8);
        return $uuid;
    }
}
